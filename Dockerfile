FROM node:6

COPY ./ /usr/src/demoapplication
WORKDIR /usr/src/demoapplication

RUN npm install

EXPOSE 3000
# EXPOSE 80 to use dns name

ENTRYPOINT ["npm","start"]
