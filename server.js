'use strict'

var path = require('path')
var fs = require('fs')

var express = require('express')
var app = express()
var app_wait = express()
var morgan = require('morgan')
var bodyParser = require('body-parser')
var methodOverride = require('method-override')

const PORT = process.env.PORT || 3000

var accessLogStream = (process.env.USE_LOG_FILE && process.env.USE_LOG_FILE.toLowerCase() === 'true') ? fs.createWriteStream(path.join('/tmp/', 'server.log'), {flags: 'a'}) : process.stdout

app.use(morgan(':remote-addr [:date[clf]] :method :url :status :res[content-length] :response-time ":user-agent"', {stream: accessLogStream}))
app.use(express.static('./public'))
app.use(bodyParser.urlencoded({'extended': 'true'}))
app.use(bodyParser.json())
app.use(bodyParser.json({type: 'application/vnd.api+json'}))
app.use(methodOverride('X-HTTP-Method-Override'))

var mysql = require('mysql2')
var AppRouter = require('./lib/api')

app_wait.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/public/waitingforconnection.html'))
})

var wait_server = app_wait.listen(PORT)

console.log('App listening on port ' + PORT)

var maxRetries = 500
var retries = 0
var delay = 500
var delayIncreaseFactor = 1.1
var maxDelay = 5

var connect = () => {

    console.log("attempting to connect to db")

    if (retries > maxRetries) {
        console.log("Ran out of retries connecting to the db")
        process.exit(1)
    }

    var connection

    connection = mysql.createPool({
        host: process.env.DB_CONNECTIONSTRING || 'localhost',
        user: process.env.DB_USERNAME || 'admin',
        password: process.env.DB_PASSWORD || 'password',
        database: process.env.DB_NAME || 'todo',
        ssl: true
    })
   connection.getConnection(function(err) {

        if (err) {

            console.error('error connecting: ' + err)//+ err.stack);
            setTimeout(connect,delay)
            retries++
            delay = delay * delayIncreaseFactor;
            console.error('waiting ' + Math.min((delay / 1000).toFixed(2),maxDelay) + ' seconds.')


        } else {
            console.log('connected as id ' + connection.threadId);
            console.log("restarting web server")

            wait_server.close(()=>{
                console.log("server restarted.")

                app.use('/api/', AppRouter(connection))
                app.listen(PORT)
            })
        }
    });
}

connect()
